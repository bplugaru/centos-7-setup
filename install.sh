#!/bin/bash
NODE_URL=http://nodejs.org/dist/node-latest.tar.gz


clear
echo "[-] Updating Packages"
yum -y update


clear
echo "[-] Upgrading Packages"
yum  -Y upgrade -y

echo 'checking if we got firewalld magic'
yum install -y firewalld #check if we have it
systemctl start firewalld

# -----------------------------------------------------

clear
echo "[-] Install NODE Packages"



echo 'lets download nodejs..'

rm -r -f /usr/local/src
mkdir -p /usr/local/src
cd /usr/local/src

wget $NODE_URL

tar -zxvf ./node-latest.tar.gz

echo 'Files extracted....'

cd node-v*

yum install -y openssl-devel gcc-c++ make git

echo 'Configuring and installing NodeJS'

./configure
make
make install


rm -f ./node-latest.tar.gz
rm -r -f /usr/local/src/node-v*


clear
echo "[-] Install MONGODB Packages"

echo '[10gen]' >> /etc/yum.repos.d/10gen.repo
echo 'name=10gen Repository' >> /etc/yum.repos.d/10gen.repo
echo 'baseurl=http://downloads-distro.mongodb.org/repo/redhat/os/x86_64' >> /etc/yum.repos.d/10gen.repo
echo 'gpgcheck=0' >> /etc/yum.repos.d/10gen.repo
echo 'enabled=1' >> /etc/yum.repos.d/10gen.repo

yum install -y mongo-10gen mongo-10gen-server

service mongod start
chkconfig mongod on
clear

echo "[-] Install AJENTI Packages"
source <(curl --insecure https://raw.githubusercontent.com/Eugeny/ajenti/master/scripts/install-rhel7.sh | sh)

yum  install ajenti-v status-v-nginx ajenti-v-nodejs  -y


service ajenti restart


firewall-cmd --permanent --zone=public --add-port=8000/tcp
firewall-cmd --reload

